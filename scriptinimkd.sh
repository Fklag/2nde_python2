#!/bin/bash
pip3 install mkdocs-material
pip3 install pymdown-extensions
pip3 install mkdocs-enumerate-headings-plugin
pip install mkdocs-minify-plugin ## attention pour ce plugin pip et non pip3

pip3 install mkdocs-macros-plugin  ## pour macro junier
pip3 install ipythonblocks  ## pour macro chambon main.py
