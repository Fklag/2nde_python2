from turtle import *

forward(100)    # On avance
left(90)        # 90 degrés à gauche
forward(50)
width(5)        # Epaisseur du trait
forward(100)
color('red')
right(90)
forward(200)

done()
#exitonclick()