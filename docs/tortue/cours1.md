---
title:  Première partie - MISE EN ROUTE
layout: parc
---

## Cours 1 - La tortue `Python`

![image](../im/ecran-tortue-0.png){ align=right width=400 }

!!! done "La tortue c'est l'ancêtre de *Scratch* !"
    En quelques lignes tu peux faire de beaux dessins.  
    

```py3
from turtle import *

forward(100)    # On avance
left(90)        # 90 degrés à gauche
forward(50)
width(5)        # Epaisseur du trait
forward(100)
color('red')
right(90)
forward(200)

done()
#exitonclick()
```

{{ basthon('scripts/tortue0.py', 800) }}

??? note "Voici une liste des principales commandes, accessibles après avoir écrit : `from turtle import *` "

    -  `forward(longueur)` avance d'un certain nombre de pas

    -  `backward(longueur)` recule

    -  `right(angle)` tourne vers la droite (sans avancer) selon un angle donné en degrés

    -   `left(angle)` tourne vers la gauche

    -   `setheading(direction)` s'oriente dans une direction ($0$ = droite, $90$ = haut, $-90$ = bas, $180$ = gauche)

    -   `goto(x,y)` se déplace jusqu'au point $(x,y)$

    -   `setx(newx)` change la valeur de l'abscisse

    -   `sety(newy)` change la valeur de l'ordonnée

    -   `down()` abaisse le stylo

    -   `up()` relève le stylo

    -   `width(epaisseur)` change l'épaisseur du trait

    -   `color(couleur)` change la couleur : , , , , ...

    -   `position()` renvoie la position $(x,y)$ de la tortue

    -   `heading()` renvoie la direction vers laquelle pointe la tortue

    -   `towards(x,y)` renvoie l'angle entre l'horizontale et le segment commençant à la tortue et finissant au point $(x,y)$

    -   `exitonclick()` termine le programme dès que l'on clique

!!! cite "Remarque"
    Les coordonnées de l'écran par défaut vont de $-475$ à $+475$ pour les $x$ et de $-400$ à $+400$ pour les $y$ ;  
    $(0,0)$ est au centre de l'écran.

<!-- \tikzstyle{line} = [draw,gray,thick]
\tikzstyle{arrow} = [->,>=latex,thick]
\begin{tikzpicture}%[scale=0.5]



\draw[arrow,orange] (-5,0)--(5.5,0) node[below]{$x$};
\draw[arrow,blue] (0,-4.25)--(0,4.5) node[left]{$y$};



\foreach \x in {-4,-3,-2,-1,1,2,3,4} {
  \draw[orange] (\x,0.1)--++(0,-0.2) node[below,scale=0.7]{$\x00$};
}

\foreach \y in {-3,-2,-1,1,2,3} {
  \draw[blue] (0.1,\y)--++(-0.2,0) node[left,scale=0.7]{$\y00$};
}

\node[above right,scale=0.7] at (0,0) {$(0,0)$};
\fill (0,0) circle (2pt);

\draw (-4.75,-4) rectangle (4.75,4);
\end{tikzpicture} -->

![image](../im/coord.png)


??? faq "Activité 1 (Premiers pas)"
    *Trace les premières lettres de `Python`, par exemple :*
    ![image](../im/ecran-tortue-1.png)
    
            
!!! tip "Objectifs : tracer tes premiers dessins."



??? faq "Activité 2 (Figures)"

    ![image](../im/ecran-tortue-2a.png){ width=10% }
    ![image](../im/ecran-tortue-2b.png){ width=15% }
    ![image](../im/ecran-tortue-2c.png){ width=20% }
    ![image](../im/ecran-tortue-2d.png){ width=25% }

    1.  **Pentagone.** Trace un premier pentagone (en bleu). Tu dois répéter
    $5$ fois : avancer de $100$ pas, tourner de $72$ degrés.

        ??? abstract "Indications."
        Pour construire une boucle utilise `for i in range(5):`  
        *(même si tu n'utilises pas ensuite la variable `i`).*

    2.  **Pentagone (bis).** Définis une variable qui vaut $200$ et une
    variable qui vaut $72$ degrés. Trace un second pentagone (en rouge)
    en avançant cette fois de et en tournant de  `angle`.

    3.  **Dodécagone.** Trace un polygone à $12$ côtés (en violet).

        ??? abstract "Indications."
        Pour tracer un polygone à $n$ côtés, il faut tourner d'un angle de $360/n$ degrés.

    4.  **Spirale.** Trace une spirale (en vert).

        ??? abstract "Indications."
        Construis une boucle, dans laquelle tu tournes toujours du même angle, mais par contre tu avances d'une longueur qui augmente à chaque étape.

!!! tip "Objectifs : tracer des figures géométriques."

??? faq "Activité 3 (Graphe de fonctions)"

    ![image](../im/ecran-tortue-3a.png){ width=30% }
    ![image](../im/ecran-tortue-3b.png){ width=30% }

    Trace le graphe de la fonction carré et de la fonction sinus.  
    Afin d'obtenir une courbe dans la fenêtre de la tortue, répète pour $x$ variant de $-200$ à $+200$ : 

    -   poser $y = \frac{1}{100} x^2$,
    -   aller à $(x,y)$.

    Pour la sinusoïde, tu peux utiliser la formule  

    $$y = 100\sin\left(\frac{1}{20}x\right).$$

    Par défaut ne connaît pas la fonction sinus, pour utiliser `sin()` il faut d'abord importer le module `math`: `from math import *`

    Pour que la tortue avance plus vite, tu peux utiliser la commande `speed("fastest")`.


!!! tip "Objectifs : tracer le graphe d’une fonction."

??? faq "Activité 4 (Triangle de Sierpinski)"

    ![image](../im/ecran-tortue-4a.png){ width=20% } ![image](../im/ecran-tortue-4b.png){ width=20% } 
    ![image](../im/ecran-tortue-4c.png){ width=20% } ![image](../im/ecran-tortue-4d.png){ width=20% } 

    Voici comment tracer le second dessin. Analyse l'imbrication des boucles et trace les dessins suivants.

    ```py3
    for i in range(3):
        color("blue")
        forward(256)
        left(120)
    
        for i in range(3):
           color("red")
           forward(128)
           left(120)
    ```

!!! tip "Objectifs : tracer le début de la fractale de Sierpinski en imbriquant des boucles."


??? faq "Activité 5 (Le cœur des tables de multiplication)."

    On fixe un entier $n$. On étudie la table de $2$, c'est-à-dire que l'on calcule $2 \times 0$, $2\times 1$, $2 \times 2$, jusqu'à $2 \times (n-1)$.  
    En plus les calculs se feront modulo $n$.  
    On calcule donc 
    
    $$2 \times k \pmod{n} \qquad \text{ pour } k=0,1,\ldots,n-1$$

    Comment dessiner cette table ?  ![image](../im/fig-tortue-5a.png){ align=right width=40% }

    On place sur un cercle $n$ points numérotés de $0$ à $n-1$. Pour chaque $k\in \{0,\ldots,n-1\}$, on relie le point numéro $k$ et le point numéro $2\times k \pmod{n}$ par un segment. 

    Voici le tracé, de la table de $2$, modulo $n=10$. 



    Par exemple :

    -   le point $3$ est relié au point $6$, car $2 \times 3 = 6$ ;

    -   le point $4$ est relié au point $8$, car $2 \times 4 = 8$ ;

    -   le point $7$ est relié au point $4$, car $2 \times 7 = 14 = 4 \pmod{10}$.

    Trace la table de $2$ modulo $n$, pour différentes valeurs de $n$.

    Voici ce que cela donne pour $n=100$.



    ![image](../im/ecran-tortue-5b.png)

    ??? abstract "Indications."
        Pour les calculs modulo $n$, utilise l'expression `(2*k) % n`.  
        Voici comment obtenir les coordonnées des sommets. Cela se fait avec les fonctions sinus et cosinus (disponibles à partir du module `math`). Les coordonnées $(x_i,y_i)$ du sommet numéro $i$, peuvent être calculées par la formule :

        $$x_i = r \cos\left(\frac{2 i \pi}{n}\right) \qquad \text{ et } \qquad y_i = r\sin\left(\frac{2 i \pi}{n}\right)$$

        Ces sommets seront situés sur le cercle de rayon $r$, centré en $(0,0)$.  
        Tu devras choisir $r$ assez grand (par exemple $r=200$).


        ![image](../im/ecran-tortue-5c.png)


<!---
    \begin{tikzpicture}

    \def\r{4};
    \def\n{10};
    \def\nn{9};

    \draw[gray,ultra thick] (0,0) circle(\r cm);
 

    \foreach \i in {0,...,9}{
    \draw[blue, very thick]  (\i*360/\n:\r) -- (2*\i*360/\n:\r);
    }

    \foreach \i in {0,...,\nn}{
    \fill[red] (\i*360/\n:\r) circle(4pt);
    \node at  (\i*360/\n:\r+0.7) {\bf \i};
    }

    \end{tikzpicture} 
-->
        
<!--    \begin{tikzpicture}

        \draw[->,>=latex,ultra thick,gray] (-5,0)--(5,0) node[below]{$x$};
        \draw[->,>=latex,ultra thick,gray] (0,-5)--(0,5) node[left]{$y$};

        \def\r{4};
        \def\n{7};


        \draw[gray,ultra thick] (0,0) circle(\r cm);
 

        \foreach \i in {0,...,\n}{
        \fill[red] (\i*360/\n:\r) circle(4pt);

        }


        \foreach \i in {0,1}{
        \node[black,above right] at  (\i*360/\n:\r){$(x_\i,y_\i)$} ;
        }


        \node[black,below right] at  (-360/\n:\r){$(x_{n-1},y_{n-1})$} ;

        \coordinate (S) at (3*360/\n:\r);
        \node[black,above left] at  (S) {$(x_{i},y_{i})$} ;
        \draw[dashed,very thick, blue] (S) -- (S |- 0,0) node[below] {$x_i = r\cos\left(\frac{2 i\pi}{n}\right)$};
        \draw[dashed,very thick, blue] (S) -- (S -| 0,0) node[right] {$y_i = r\sin\left(\frac{2 i\pi}{n}\right)$};


        \node[black,below right] at  (0,0) {$(0,0)$} ;
        \draw[gray,very thick, below right]  (0,0)--(-140:\r) node[midway, below right,black] {$r$} ;
        \end{tikzpicture} 
-->

!!! tip "Objectifs : dessiner les tables de multiplication."
    Pour une introduction en vidéo voir « [la face cachée des tables de multiplication](https://www.yout-ube.com/watch?v=-X49VQgi86E) » sur *Youtube* par Micmaths.
