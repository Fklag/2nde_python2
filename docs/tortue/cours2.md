---
title:  Première partie - MISE EN ROUTE
layout: parc
---

## Cours 2 - Plusieurs tortues

On peut définir plusieurs tortues qui se déplacent de façon indépendante chacune de leur côté.  
Voici comment définir deux tortues (une rouge et une bleue) et les déplacer.

```py3
from turtle import *

tortue1 = Turtle()    # Avec un 'T' majuscule !
tortue2 = Turtle()

tortue1.color('red')
tortue2.color('blue')

tortue1.forward(100)
tortue2.left(90)
tortue2.forward(100)

done()
#exitonclick()
```

{{ basthon('scripts/tortue1.py', 800) }}

??? faq "Activité 6 (La poursuite des tortues)"

    Programme quatre tortues qui courent les unes après les autres :

    ![image](../im/ecran-tortue-6a.png){ align=right width=40% }

    -   la tortue 1 court après la tortue 2,

    -   la tortue 2 court après la tortue 3,

    -   la tortue 3 court après la tortue 4,

    -   la tortue 4 court après la tortue 1.

    Voici les positions et orientations de départ :

    ![image](../im/fig-tortue-6c.png)


    ??? abstract "Indications."
        Utilise le bout de code suivant :

        ```py3
        position1 = tortue1.position()
        position2 = tortue2.position()
        angle1 = tortue1.towards(position2)
        tortue1.setheading(angle1)
        ```

        -   Tu places les tortues aux quatre coins d'un carré,  
            par exemple en $(-200,-200)$, $(200,-200)$, $(200,200)$ et $(-200,200)$.

        -   Tu récupères la position de la première tortue par `position1 = tortue1.position()`.  
        Idem pour les autres tortues.

        -   Tu calcules l'angle entre la tortue 1 et la tortue 2 par la commande `angle1 = tortue1.towards(position2)` .
    
        -   Tu orientes la tortue 1 selon cet angle : `tortue1.setheading(angle1)`.

        -   Tu avances la tortue 1 de $10$ pas.

    Améliore ton programme en traçant à chaque fois un segment entre la tortue poursuivante et la tortue poursuivie.

    ![image](../im/ecran-tortue-6b.png)


<!-- \tikzstyle{line} = [draw,gray,very thick]
\tikzstyle{arrow} = [->,>=latex,ultra thick]
\begin{tikzpicture}[scale=0.5]

\draw[line] (-5,-5) rectangle ++(10,10);
\draw[arrow,red] (-5,-5) -- ++(3,0);
\draw[arrow,green!60!black] (-5,5) -- ++(0,-3);
\draw[arrow,orange] (5,5) -- ++(-3,0);
\draw[arrow,blue] (5,-5) -- ++(0,3);

\fill[red] (-5,-5)  circle (7pt) node[below left, black] {tortue 1};
\fill[green!60!black] (-5,5)  circle (7pt) node[above left, black] {tortue 4};
\fill[orange] (5,5)  circle (7pt) node[above right, black] {tortue 3};
\fill[blue] (5,-5)  circle (7pt) node[below right, black] {tortue 2};

\end{tikzpicture} -->


!!! tip "Objectifs : tracer des courbes de poursuite."
