---
title:  Première partie - MISE EN ROUTE
layout: parc
---

??? info "Vidéos $\blacksquare$ Premiers pas"


    > [Vidéo $\blacksquare$ Tortue - partie 1](http://www.yout-ube.com/watch?v=OpJb6ur4JAk)

    > [Vidéo $\blacksquare$ Tortue - partie 2](http://www.yout-ube.com/watch?v=7Fb_FOp5NGs)


# Tortue (Scratch avec Python)

!!! tip "Objectifs : Le module `turtle` permet de tracer facilement des dessins en `Python`"
    Il s'agit de commander une tortue à l'aide d'instructions simples comme "avancer", "tourner"...  
    C'est le même principe qu'avec *Scratch*, avec toutefois des différences : tu ne déplaces plus des blocs, mais tu écris les instructions ; et en plus les instructions sont en anglais !



!!! warning "Suivre les activités en ligne ci-dessous"
    
!!! abstract "Dans cette toute première activité"
    La tortue c’est l’ancêtre de Scratch ! En quelques lignes tu peux faire de beaux dessins.   
    *-> pour consulter le tout en pdf : [2nde_python2.pdf](./2nde_python2.pdf)*


    [Cours 1 - La tortue `Python`](tortue/cours1.md){ .md-button }

    [Cours 2 - Plusieurs tortues](tortue/cours2.md){ .md-button }